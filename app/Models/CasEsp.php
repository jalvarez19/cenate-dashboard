<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CasEsp extends Model
{
    use HasFactory;
    protected $table= 'cas_esp';
    protected $fillable = [
        'id',
    ];
    
}
