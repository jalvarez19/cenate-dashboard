<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Asistencial extends Model
{
    use HasFactory;
    protected $table= 'personal_asistencial';
    protected $fillable = [
        'id',
    ];
}
