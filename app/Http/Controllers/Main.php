<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CasEsp;
use App\Models\Asistencial;
use App\Models\Cas;

class Main extends Controller
{
    public function index(){
        $cas = Cas::where('estado', 1)->get();
        return view('welcome', compact('cas'));
    }
    public function getEspeCas(){
        $data = CasEsp::all();
        return json_encode($data);
    }
    public function getcookie(){
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'http://172.20.0.210:8080/explotacionDatos/servlet/Index',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => array('USER' => '46852216','PASS' => 'nodaracenate','centroAsistencial' => '500','udp' => 'indexCas','opt' => '0'),
          CURLOPT_HTTPHEADER => array(
            'Accept: 172.20.0.210:8080',
            'Accept-Encoding: gzip, deflate',
            'Accept-Language: es-419,es;q=0.9',
            'Cache-Control: max-age=0',
            'Connection: keep-alive',
            'Content-Length: 63',
            'Content-Type: application/x-www-form-urlencoded',
            'Host: 172.20.0.210:8080',
            'Origin: http://172.20.0.210:8080',
            'Referer: http://172.20.0.210:8080/explotacionDatos/index1.html',
            'Upgrade-Insecure-Requests: 1',
            'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36',
          ),
        ));
        $result = curl_exec($curl);
        dd($result);
        preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $result, $matches);
        $cookies = array();
        foreach($matches[1] as $item) {
            parse_str($item, $cookie);
            $cookies = array_merge($cookies, $cookie);
        }
        return $cookies;
        

    }
    public function post_explota(Request $request){
        $session = $request->sesion;
        $fecha_inicial = strtotime($request->inicio);
        $dia_ini = strftime("%d",$fecha_inicial);
        $mes_ini = strftime("%m",$fecha_inicial);
        $ani_ini = strftime("%Y",$fecha_inicial);
        $fecha_bd = $dia_ini.'/'.$mes_ini.'/'.$ani_ini;
        $fechaini = $dia_ini.'%2F'.$mes_ini.'%2F'.$ani_ini;
        $datos = CasEsp::where('fec_cita', $fecha_bd)->delete();
        $cas = Cas::where('estado', 1)->get();
        $i = 0;
        foreach ($cas as $centro) {
            $cas = $centro->id;
            $length = 3;
            $cas = substr(str_repeat(0, $length).$cas, - $length);
            $curl = curl_init();
            curl_setopt_array($curl, array(
              CURLOPT_URL => 'http://172.20.0.210:8080/explotacionDatos/servlet/CtrlControl?opt=adm116_xls',
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'POST',
              CURLOPT_POSTFIELDS => 'servicio=00&actividad=00&subactividad=00&tipoDocumento=00&numeroDocumento=&fechaInicio='.$fechaini.'&fechaFin='.$fechaini.'&formatoArchivo=xls&CAS='.$cas.'&ORIGEN=1',
              CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded',
                'Cookie: '.$session.''
              ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            $arr = explode( "|", $response);
            $data = [];
            foreach($arr as $ar){
                if (strlen($ar) > 2) {
                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                      CURLOPT_URL => 'http://172.20.0.210:8080/explotacionDatos/servlet/CtrlControl?opt=adm116_xls_descarga&fn='.$ar,
                      CURLOPT_RETURNTRANSFER => true,
                      CURLOPT_ENCODING => '',
                      CURLOPT_MAXREDIRS => 10,
                      CURLOPT_TIMEOUT => 0,
                      CURLOPT_FOLLOWLOCATION => true,
                      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                      CURLOPT_CUSTOMREQUEST => 'POST',
                      CURLOPT_HTTPHEADER => array(
                        'Cookie: '.$session.''
                      ),
                    ));
                    $response = curl_exec($curl);
                    curl_close($curl);
                    $comp = preg_split('/\r\n|\r|\n/', $response);
                    $data = [];
                    foreach($comp as $co){
                        $array = explode('|', $co);
                        if ($array[0] == 'CENTRO') {
                            
                        }else{
                            if(count($array) > 5){
                                $asistencial = Asistencial::where('num_doc', $array[8])->where('dia', $fecha_bd)->first();
                                if ($asistencial) {
                                    $casesp = new CasEsp();
                                    $casesp->cas = $cas;
                                    $casesp->dni_med = $array[8];
                                    $casesp->nom_med = $array[9];
                                    $casesp->dni_pac = $array[14];
                                    $casesp->estado = $array[23];
                                    $casesp->fec_cita = $array[10];
                                    $casesp->especialidad = $array[3];
                                    $casesp->turno = $array[37];
                                    $casesp->mot_des = $array[48];
                                    $casesp->save();
                                    $i++;
                                }
                            }else{
                                
                            }
                        }
                    }
                }
            }
        }
        if ($i < 1) {
            return redirect('/')->withErrors(['Error en carga día: '.$fecha_bd]);
        }else{
            return redirect('/')->withErrors(['Carga día: '.$fecha_bd.', cargados: '.$i]);
        }
        
    }
}
