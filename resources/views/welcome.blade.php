<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Responsive Admin Dashboard Template">
        <meta name="keywords" content="admin,dashboard">
        <meta name="author" content="stacks">
        <!-- The above 6 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        
        <!-- Title -->
        <title>CENATE - ESTADÍSTICAS</title>

        <!-- Styles -->
        <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,700,800&display=swap" rel="stylesheet">
        <link href="/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="/assets/plugins/font-awesome/css/all.min.css" rel="stylesheet">
        <link href="/assets/plugins/perfectscroll/perfect-scrollbar.css" rel="stylesheet">

      
        <!-- Theme Styles -->
        <link href="/assets/css/main.min.css" rel="stylesheet">
        <link href="/assets/css/dark-theme.css" rel="stylesheet">
        <link href="/assets/css/custom.css" rel="stylesheet">


    </head>
    <body class="login-page">
        <div class='loader'>
            <div class='spinner-grow text-primary' role='status'>
              <span class='sr-only'>Loading...</span>
            </div>
          </div>
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-md-6">
                    <div class="" style="margin-top: 20px;">
                        <div class="card-body">
                            <div class="authent-logo">
                                <img src="/assets/images/logo@2x.png" alt="">
                            </div>
                            <div class="authent-text">
                                <p>Panel de Estadísticas</p>
                                <p>CENATE</p>
                            </div>

                            <form method="POST" action="/explotar">
                                <div class="mb-3">
                                    @if(count($errors) > 0)
                                     @foreach( $errors->all() as $message )
                                      <div class="alert alert-info display-hide">
                                       <span>{{ $message }}</span>
                                      </div>
                                     @endforeach
                                    @endif
                                </div>
                                @csrf
                                <div class="mb-3">
                                    <div class="form-floating">
                                        <input type="date" class="form-control" id="inicio" name="inicio" placeholder="Fecha">
                                        <label for="inicio">Fecha</label>
                                      </div>
                                </div>
                                <div class="mb-3">
                                    <div class="form-floating">
                                        <select class="form-control" id="tipdescarga" name="tipdescarga" style="color: #d7d7d7">
                                            <option>Citas por especialidades</option>
                                        </select>
                                        <label for="tipdescarga">Descarga</label>
                                      </div>
                                </div>
                                <div class="mb-3">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" id="sesion" name="sesion" placeholder="Sesión">
                                        <label for="sesion">Sesión</label>
                                      </div>
                                </div>
                                <div class="d-grid">
                                    <button type="submit" class="btn btn-info m-b-xs">Explotar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <table class="table">
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Centro</th>
                          <th scope="col">Estado</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($cas as $centro)
                        <tr>
                          <th scope="row" style="font-size: 11px;">{{$centro->id}}</th>
                          <td style="font-size: 11px;">{{$centro->nombre}}</td>
                          @if($centro->estado == 1)
                          <td><span class="badge rounded-pill bg-success">Activo</span></td>
                          @else
                          <td><span class="badge rounded-pill bg-danger">Inactivo</span></td>
                          @endif
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
         
        
        <!-- Javascripts -->
        <script src="/assets/plugins/jquery/jquery-3.4.1.min.js"></script>
        <script src="https://unpkg.com/@popperjs/core@2"></script>
        <script src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="https://unpkg.com/feather-icons"></script>
        <script src="/assets/plugins/perfectscroll/perfect-scrollbar.min.js"></script>
        <script src="/assets/js/main.min.js"></script>
    </body>
</html>