<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Main;

Route::get('/', [Main::class, 'index']);

Route::post('/explotar', [Main::class, 'post_explota']);
Route::get('/getcookie', [Main::class, 'getcookie']);

Route::get('/apiEspecialidadCas', [Main::class, 'getEspeCas']);
